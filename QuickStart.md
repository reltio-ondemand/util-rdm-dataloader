# Quick Start 

## Building

The path to build the RDM DataLoader is 
rdm-data-loader\src\main\java\com\reltio\rdm\dataload\service\LoadJsonToRDMTenant.java

## Dependencies 

1. reltio-cst-core-1.4.6


## Parameters and Properties File Example

```
#!paintext
#Common Properties
ENVIRONMENT_URL=dev.appspot.com
AUTH_URL=https://auth.reltio.com/oauth/token
TENANT_ID=***************
USERNAME=**************
PASSWORD=**************

#Tool specific properties
JSON_FILE=//Projects//Json//CTRY_SB_DVSN_CD.json
#Default Record Per Post = 50
RECORDS_PER_POST=100

#Default THREAD_COUNT = 5
THREAD_COUNT=10

```

## JSON File Example - CTRY_SB_DVSN_CD.json
### Each Line should be a JSON array

```
#!paintext
[
  {
    "tenantId": "************",
    "code": "AD",
    "type": "rdm/lookupTypes/CTRY_CD",
    "sourceMappings": [
      {
        "source": "Reltio",
        "values": [
          {
            "code": "AD",
            "value": "Andorra"
          }
        ]
      },
      {
        "source": "DATALOADER",
        "values": [
          {
            "code": "AD",
            "value": "Andorra"
          }
        ]
      },
      {
        "source": "WEBSERVICE",
        "values": [
          {
            "code": "AD",
            "value": "Andorra"
          }
        ]
      },
      {
        "source": "MIGRATION",
        "values": [
          {
            "code": "AD",
            "value": "Andorra"
          }
        ]
      }
    ],
    "parents": [
      "rdm/lookupTypes/WRLD_RGN/6004",
      "rdm/lookupTypes/GRPHY_HRCHY_LVL_CLSFN_CD/WR"
    ],
    "attributes": [
      {
        "name": "CTRY_CD",
        "value": "AD"
      },
      {
        "name": "MDCP_DLT_IND",
        "value": "N"
      },
      {
        "name": "CTRY_TEL_ACSS_CD",
        "value": "376"
      },
      {
        "name": "CTRY_DL_OUT_NR",
        "value": "0"
      },
      {
        "name": "XTRN_IND",
        "value": "Y"
      }
    ]
  }
]


```

## Executing

Command to start the utility.
```
#!plaintext

java -jar rdm-data-loader-{$version}.jar  dataload.properties > $logfilepath$


```
