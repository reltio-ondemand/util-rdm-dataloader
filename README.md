
#RDM JSON DATALOADER

## Description
The RDM Data Loader is a tool for bulk loading lookups into a Reltio’s RDM tenant in JSON form. The tool is commonly used in sequence following the RDM JSON Generator tool to prepare data for loading.

##Change Log

```
#!plaintext

Last Update Date: 7 March 2022 
Version         : 2.7
Description     : Updated Reltio CST Core jar version to 1.5.1

Last Update Date: 22 December 2021 
Version         : 2.6
Description     : Updated the Log4j dependency to 2.17.0. Log4j v2.17.0 has fixes for the following vulnerabilities CVE-2021-44228, CVE-2021-45046, CVE-2021-45105

Last Update Date: 12 February 2021 
Version         : 2.5
Description     : Updated the Reltio CST Core Version to 1.4.9.  Implemented CLIENT_CREDENTIALS.

Last Update Date: 02 July 2019 
Version         : 2.4
Description     : Standarization of Jar name (https://reltio.jira.com/browse/ROCS-38)
                : Proxy Change (https://reltio.jira.com/browse/ROCS-37)

Last Update Date: 28/03/2019
Version: 2.3
Description: Implemented encrypt the password stored in properties file and Clear validation message when properties are missing

Last Update Date: 10/12/2018
Version: 2.2
Description: Upgraded the Reltio CST Core Version. Added Performance Logs

Last Update Date: 03/08/2018
Version: 2.1.0
Description: Upgraded the Reltio CST Core Version. Renamed DATALOAD_SERVER_HOST to ENVIRONMENT_URL and renamed JSON_FILE_PATH to JSON_FILE.
```
##Contributing 
Please visit our [Contributor Covenant Code of Conduct](https://bitbucket.org/reltio-ondemand/common/src/a8e997d2547bf4df9f69bf3e7f2fcefe28d7e551/CodeOfConduct.md?at=master&fileviewer=file-view-default) to learn more about our contribution guidlines

## Licensing
```
#!plaintext
Copyright (c) 2017 Reltio

 

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

 

    http://www.apache.org/licenses/LICENSE-2.0

 

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

See the License for the specific language governing permissions and limitations under the License.
```

## Quick Start 
To learn about dependencies, building and executing the tool view our [quick start](https://bitbucket.org/reltio-ondemand/util-rdm-dataloader/src/master/QuickStart.md).


