package com.reltio.rdm.dataload.constants;

import com.google.gson.Gson;

/**
 * Created by gowganesh on 13/06/17.
 */
public final class RDMDataloadConstants {

    public static final Integer RECORDS_PER_POST = 50;
    public static final Integer THREAD_COUNT = 5;
    public static final Gson GSON = new Gson();
    public static final Integer MAX_QUEUE_SIZE_MULTIPLICATOR = 10;
}
