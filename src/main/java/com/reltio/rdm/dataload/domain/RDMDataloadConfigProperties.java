package com.reltio.rdm.dataload.domain;

import com.reltio.rdm.dataload.constants.RDMDataloadConstants;

import java.io.Serializable;
import java.util.Properties;

/**
 * Created by gowganesh on 13/06/17.
 */
public class RDMDataloadConfigProperties implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8219901602034740714L;
    private String fileName;
    private String serverHostName;
    private String tenantId;
    private String username;
    private String password;
    private String clientCredentials;
    private String authURL;
    private String baseDataloadURL;
    private Integer threadCount;
    private Integer groupsCount;


    public RDMDataloadConfigProperties(Properties properties) {

        fileName = properties.getProperty("JSON_FILE");
        serverHostName = properties.getProperty("ENVIRONMENT_URL");
        tenantId = properties.getProperty("TENANT_ID");
        username = properties.getProperty("USERNAME");
        password = properties.getProperty("PASSWORD");
        authURL = properties.getProperty("AUTH_URL");

        if (checkNull(serverHostName) && checkNull(tenantId)) {
            baseDataloadURL = "https://" + serverHostName + "/lookups/"
                    + tenantId;
        }


        if (!checkNull(properties.getProperty("RECORDS_PER_POST"))) {
            groupsCount = RDMDataloadConstants.RECORDS_PER_POST;
        } else {
            groupsCount = Integer.parseInt(properties
                    .getProperty("RECORDS_PER_POST"));
        }
        if (!checkNull(properties.getProperty("MAIL_TRANSPORT_PROTOCOL"))) {
            if (!checkNull(properties.getProperty("THREAD_COUNT"))) {
                threadCount = RDMDataloadConstants.THREAD_COUNT;
            } else {
                threadCount = Integer.parseInt(properties
                        .getProperty("THREAD_COUNT"));
            }
        }
    }

    public static boolean checkNull(String value) {
        if (value != null && !value.trim().equals("")
                && !value.trim().equals("UNKNOWN")
                && !value.trim().equals("<blank>")
                && !value.trim().equals("<UNAVAIL>")
                && !value.trim().equals("#")
                && !value.toLowerCase().trim().equals("null")
                && !value.toLowerCase().trim().equals("\"")) {
            return true;
        }
        return false;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getServerHostName() {
        return serverHostName;
    }

    public void setServerHostName(String serverHostName) {
        this.serverHostName = serverHostName;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAuthURL() {
        return authURL;
    }

    public void setAuthURL(String authURL) {
        this.authURL = authURL;
    }

    public String getBaseDataloadURL() {
        return baseDataloadURL;
    }

    // Function to ignore null values

    public void setBaseDataloadURL(String baseDataloadURL) {
        this.baseDataloadURL = baseDataloadURL;
    }

    /**
     * @return the groupsCount
     */
    public Integer getGroupsCount() {
        return groupsCount;
    }

    /**
     * @param groupsCount the groupsCount to set
     */
    public void setGroupsCount(Integer groupsCount) {
        this.groupsCount = groupsCount;
    }

    /**
     * @return the threadCount
     */
    public Integer getThreadCount() {
        return threadCount;
    }

    /**
     * @param threadCount the threadCount to set
     */
    public void setThreadCount(Integer threadCount) {
        this.threadCount = threadCount;
    }

	public String getClientCredentials() {
		return clientCredentials;
	}

	public void setClientCredentials(String clientCredentials) {
		this.clientCredentials = clientCredentials;
	}
}
