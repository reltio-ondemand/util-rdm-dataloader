package com.reltio.rdm.dataload.service;

import com.google.gson.reflect.TypeToken;
import com.reltio.cst.exception.handler.APICallFailureException;
import com.reltio.cst.exception.handler.GenericException;
import com.reltio.cst.exception.handler.ReltioAPICallFailureException;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.service.TokenGeneratorService;
import com.reltio.cst.service.impl.SimpleReltioAPIServiceImpl;
import com.reltio.cst.service.impl.TokenGeneratorServiceImpl;
import com.reltio.cst.util.Util;
import com.reltio.file.ReltioFileReader;
import com.reltio.file.ReltioFlatFileReader;
import com.reltio.rdm.dataload.constants.RDMDataloadConstants;
import com.reltio.rdm.dataload.domain.RDMDataloadConfigProperties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created by gowganesh on 13/06/17.
 */
public class LoadJsonToRDMTenant {

    private static final Logger logger = LogManager.getLogger(LoadJsonToRDMTenant.class.getName());
    private static final Logger logPerformance = LogManager.getLogger("performance-log");

    public static void main(String[] args) throws Exception {

        int count = 0;
        long totalFuturesExecutionTime = 0l;
        Properties config = new Properties();
        Long programStartTime = System.currentTimeMillis();
        logger.info("Load process started...");
        try {
            String propertyFilePath = args[0];
            config = Util.getProperties(propertyFilePath, "PASSWORD", "CLIENT_CREDENTIALS");
        } catch (Exception e) {
            logger.error("Failed to Read the Properties File :: ");
            e.printStackTrace();
        }
        
        Map<List<String>, List<String>> mutualExclusiveProps = new HashMap<>();
		mutualExclusiveProps.put(Arrays.asList("PASSWORD","USERNAME"), Arrays.asList("CLIENT_CREDENTIALS"));
		List<String> missingKeys = Util.listMissingProperties(config,
                Arrays.asList("JSON_FILE", "ENVIRONMENT_URL", "AUTH_URL", "TENANT_ID"), mutualExclusiveProps);

        if (!missingKeys.isEmpty()) {
        	logger.error("Following properties are missing from configuration file!! \n" + missingKeys);
            System.exit(0);
        }
        
        final RDMDataloadConfigProperties configProperties = new RDMDataloadConfigProperties(config);

        if (args != null && args.length > 1) {
            configProperties.setFileName(args[1]);
        }

        //Proxy setup
        //if (!Util.isEmpty(config.getProperty("HTTP_PROXY_HOST")) &&
        //        !Util.isEmpty(config.getProperty("HTTP_PROXY_PORT"))) {
        //    Util.setHttpProxy(config);
        //}

        Util.setHttpProxy(config);
        
        ThreadPoolExecutor executorService = (ThreadPoolExecutor) Executors
                .newFixedThreadPool(configProperties.getThreadCount());

        ReltioFileReader fileReader = null;
        fileReader = new ReltioFlatFileReader(configProperties.getFileName(), null, "UTF-8");

		final ReltioAPIService reltioAPIService = Util.getReltioService(config);

        boolean eof = false;
        ArrayList<Future<Long>> futures = new ArrayList<Future<Long>>();
        while (!eof) {

            List<Object> inputRecords = new ArrayList<>();

            for (int threadNum = futures.size(); threadNum < configProperties.getThreadCount() * RDMDataloadConstants.MAX_QUEUE_SIZE_MULTIPLICATOR; threadNum++) {
                inputRecords.clear();
                for (int k = 0; k < configProperties.getGroupsCount(); k++) {
                    String[] nextHcp = null;

                    try {
                        nextHcp = fileReader.readLine();
                    } catch (Exception e) {
                        e.printStackTrace();
                        nextHcp = fileReader.readLine();
                    }
                    if (nextHcp == null) {
                        eof = true;
                        break;
                    }

                    try {
                        List<Object> recordsInLine = RDMDataloadConstants.GSON.fromJson(
                                nextHcp[0],
                                new TypeToken<List<Object>>() {
                                }.getType());
                        inputRecords.addAll(recordsInLine);
                        count = count + recordsInLine.size();
                    } catch (Exception e) {
                        count = count + 1;
                        logger.error(
                                "Invalid JSON|" + nextHcp[0]);
                    }


                }

                if (inputRecords.size() > 0) {
                    final List<Object> totalRecordsSent = new ArrayList<>();
                    totalRecordsSent.addAll(inputRecords);
                    final String stringToSend = RDMDataloadConstants.GSON.toJson(totalRecordsSent);

                    futures.add(executorService.submit(new Callable<Long>() {
                        @Override
                        public Long call() {
                            long requestExecutionTime = 0l;
                            long startTime = System.currentTimeMillis();

                            try {
                                String response = sendLookups(configProperties.getBaseDataloadURL(), stringToSend, reltioAPIService);
                                List<Object> dataloadResponses = RDMDataloadConstants.GSON
                                        .fromJson(
                                                response,
                                                new TypeToken<List<Object>>() {
                                                }.getType());
                                logger.debug("Success|Records Sent = " + totalRecordsSent.size() + "Response Received = " + dataloadResponses.size() + "|" + response);
                            } catch (ReltioAPICallFailureException e) {
                                logger.error("Failure|" + e.getMessage() + "|" + stringToSend);
                                e.printStackTrace();
                            } catch (GenericException e) {
                                logger.error("Failure|" + e.getMessage() + "|" + stringToSend);
                                e.printStackTrace();
                            }

                            requestExecutionTime = System.currentTimeMillis()
                                    - startTime; // one

                            return requestExecutionTime;
                        }
                    }));
                }
            }
            totalFuturesExecutionTime += waitForTasksReady(futures,
                    configProperties.getThreadCount()
                            * (RDMDataloadConstants.MAX_QUEUE_SIZE_MULTIPLICATOR / 2));

            printDataloadPerformance(executorService.getCompletedTaskCount()
                            * configProperties.getGroupsCount(),
                    totalFuturesExecutionTime, totalFuturesExecutionTime,
                    programStartTime,
                    configProperties.getThreadCount());
        }
        totalFuturesExecutionTime += waitForTasksReady(futures, 0);

        // System.out.println("Queues are empty. Printing final results");
        printDataloadPerformance(executorService.getCompletedTaskCount()
                        * configProperties.getGroupsCount(),
                totalFuturesExecutionTime, totalFuturesExecutionTime,
                programStartTime,
                configProperties.getThreadCount());

        executorService.shutdown();
        fileReader.close();
        logger.info("Load process completed...");
    }

    public static void printDataloadPerformance(long totalTasksExecuted,
                                                long totalTasksExecutionTime, long totalQueueWaitTime,
                                                long programStartTime, long numberOfThreads) {
        logger.info("[Performance]: ============= Current performance status ("
                + new Date().toString() + ") =============");
        long finalTime = System.currentTimeMillis() - programStartTime;
        logger.info("[Performance]:  Total processing time : "
                + finalTime);
        logger.info("[Performance]:  Total queue waiting time : "
                + totalQueueWaitTime);
        logger.info("[Performance]:  Entities sent: "
                + totalTasksExecuted);
        logger.info("[Performance]:  Total OPS (Entities sent / Time spent from program start): "
                + (totalTasksExecuted / (finalTime / 1000f)));
        logger.info("[Performance]:  Total OPS without waiting for queue (Entities sent / (Time spent from program start - Time spent in waiting for API queue)): "
                + (totalTasksExecuted / ((finalTime - totalQueueWaitTime) / 1000f)));
        logger.info("[Performance]:  API Server data load requests OPS (Entities sent / (Sum of time spend by API requests / Threads count)): "
                + (totalTasksExecuted / ((totalTasksExecutionTime / numberOfThreads) / 1000f)));
        logger.info("[Performance]: ===============================================================================================================");

        //log performance only in separate logs
        logPerformance.info("[Performance]: ============= Current performance status ("
                + new Date().toString() + ") =============");
        logPerformance.info("[Performance]:  Total processing time : "
                + finalTime);
        logPerformance.info("[Performance]:  Total queue waiting time : "
                + totalQueueWaitTime);
        logPerformance.info("[Performance]:  Entities sent: "
                + totalTasksExecuted);
        logPerformance.info("[Performance]:  Total OPS (Entities sent / Time spent from program start): "
                + (totalTasksExecuted / (finalTime / 1000f)));
        logPerformance.info("[Performance]:  Total OPS without waiting for queue (Entities sent / (Time spent from program start - Time spent in waiting for API queue)): "
                + (totalTasksExecuted / ((finalTime - totalQueueWaitTime) / 1000f)));
        logPerformance.info("[Performance]:  API Server data load requests OPS (Entities sent / (Sum of time spend by API requests / Threads count)): "
                + (totalTasksExecuted / ((totalTasksExecutionTime / numberOfThreads) / 1000f)));
        logPerformance.info("[Performance]: ===============================================================================================================");
    }

    /**
     * Waits for futures (load tasks list put to executor) are partially ready.
     * <code>maxNumberInList</code> parameters specifies how much tasks could be
     * uncompleted.
     *
     * @param futures         - futures to wait for.
     * @param maxNumberInList - maximum number of futures could be left in "undone" state.
     * @return sum of executed futures execution time.
     */
    public static long waitForTasksReady(Collection<Future<Long>> futures,
                                         int maxNumberInList) {
        long totalResult = 0l;
        while (futures.size() > maxNumberInList) {
            try {
                Thread.sleep(20);
            } catch (Exception e) {
                // ignore it...
            }
            for (Future<Long> future : new ArrayList<Future<Long>>(futures)) {
                if (future.isDone()) {
                    try {
                        totalResult += future.get();
                        futures.remove(future);
                    } catch (Exception e) {
                        logger.error(e.getMessage());
                        logger.debug(e);
                    }
                }
            }
        }
        return totalResult;
    }

    public static String sendLookups(String srcUrl, String stringToSend,
                                     ReltioAPIService reltioAPIService) throws GenericException,
            ReltioAPICallFailureException {
        String response = null;
        response = reltioAPIService.post(srcUrl, stringToSend);

        if (response == null) {
            throw new GenericException("Empty Response Received...");
        }
        return response;

    }
}
