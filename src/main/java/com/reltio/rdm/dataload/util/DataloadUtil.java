package com.reltio.rdm.dataload.util;

/**
 * Created by gowganesh on 13/06/17.
 */
public class DataloadUtil {

    // Function to ignore null values

    public static boolean checkNull(String value) {
        if (value != null && !value.trim().equals("")
                && !value.trim().equals("UNKNOWN")
                && !value.trim().equals("<blank>")
                && !value.trim().equals("<UNAVAIL>")
                && !value.trim().equals("#")
                && !value.toLowerCase().trim().equals("null")
                && !value.toLowerCase().trim().equals("\"")) {
            return true;
        }
        return false;
    }
}
